import mongoose from 'mongoose';
import DecisionTableRepo from '../repos/decisionTable-repo';
import JsFeelUtils from '../utils/jsFeel';

/**
 * DecisionTable schema definition
 */

const decisionTableSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true, index: true },
  decisionRules: { type: String },
  documentData: { type: String },
  documentName: { type: String },
  dtDesignerData: { type: Object },
});

export const DecisionTableModel = mongoose.model(
  'DecisionTable',
  decisionTableSchema
);

/**
 * Class representation of DecisionTable.
 */

export default class DecisionTable {
  constructor(decisionTable) {
    this._decisionTable = decisionTable;
  }

  get name() {
    return this._decisionTable.name;
  }

  get decisionRules() {
    return this._decisionTable.decisionRules;
  }

  get documentData() {
    return this._decisionTable.documentData;
  }

  get documentName() {
    return this._decisionTable.documentName;
  }

  get dtDesignerData() {
    return this._decisionTable.dtDesignerData;
  }

  createDecisionTable = async () => {
    return await new DecisionTableRepo()
      .create(this._decisionTable)
      .catch((err) => {
        throw err;
      });
  };

  static findDecisionTableById = async (id) => {
    return await new DecisionTableRepo().findById(id).catch((err) => {
      throw err;
    });
  };

  static findAllDecisionTables = async (cond, proj) => {
    return await new DecisionTableRepo().findAll(cond, proj).catch((err) => {
      throw err;
    });
  };

  static findOneDecisionTables = async (cond, proj) => {
    return await new DecisionTableRepo().findOne(cond, proj).catch((err) => {
      throw err;
    });
  };

  static exec = async (name, args) => {
    try {
      const { id, decisionRules } = await this.findOneDecisionTables({ name });
      const resp = await JsFeelUtils.getDecision(
        id,
        JSON.parse(decisionRules),
        args
      );
      return resp;
    } catch (error) {
      throw error;
    }
  };
}
