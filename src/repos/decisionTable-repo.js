import BaseRepo from './base-repo';
import { DecisionTableModel } from '../models/decisionTable';
export default class DecisionTableRepo extends BaseRepo {
  constructor() {
    super(DecisionTableModel);
  }
}

Object.seal(DecisionTableRepo.prototype.constructor);
