export default class BaseRepo {
  constructor(model) {
    this._model = model;
  }

  findById = async (id) => {
    return await this._model.findById(id).catch((err) => {
      throw err;
    });
  };

  findOne = async (cond = {}, proj = {}) => {
    return await this._model.findOne(cond, proj).catch((err) => {
      throw err;
    });
  };

  findAll = async (cond = {}, proj = {}) => {
    return await this._model.find(cond, proj).catch((err) => {
      throw err;
    });
  };

  create = async (item) => {
    return await this._model.create(item).catch((err) => {
      throw err;
    });
  };
}
