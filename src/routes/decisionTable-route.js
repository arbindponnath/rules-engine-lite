import express from 'express';
import DecisionTableController from '../controllers/decisionTable-controller';
/**
 * Class representation of DecisionTable route.
 */

class DecisionTableRoute {
  router = express.Router();

  constructor() {
    this.router.post('/decisionTables', DecisionTableController.create);
    this.router.get('/decisionTables', DecisionTableController.findAll);
    this.router.get('/decisionTables/:id', DecisionTableController.findById);
    this.router.post('/decisionTables/exec', DecisionTableController.exec);
  }
}

export default new DecisionTableRoute().router;
