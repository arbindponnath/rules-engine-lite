import App from './server.js';

const appConfig = { port: 8081, dbName: 'starterJSDB' };
new App(appConfig).launch();
