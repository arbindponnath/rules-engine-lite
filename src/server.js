import express from 'express';
import helmet from 'helmet';
import mongoose from 'mongoose';
import cors from 'cors';
import DecisionTableRouter from './routes/decisionTable-route';

export default class App {
  app = express();
  constructor(appConfig) {
    this.port = appConfig.port;
    this.hostname = appConfig.hostname || 'localhost';
    this.dbHost = appConfig.dbHost || 'localhost';
    this.dbPort = appConfig.dbPort || 27017;
    this.dbName = appConfig.dbName;
    this.appName = appConfig.appName || 'Express starter app (JS)';
    this._init();
  }

  _init() {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(helmet());
    this._initRoutes();
  }

  _initRoutes() {
    this.app.get('/', (req, res, next) => {
      res.status(200).send(`Welcome to ${this.appName}`);
    });
    this.app.use('/api', DecisionTableRouter);
  }

  launch() {
    const uri = `mongodb://${this.dbHost}:${this.dbPort}/${this.dbName}`;
    mongoose.connect(
      uri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err) => {
        if (err) console.error('DB connection failed.');
        else
          this.app.listen(this.port, () => {
            console.log(`Server started on port ${this.port}`);
          });
      }
    );
  }
}
