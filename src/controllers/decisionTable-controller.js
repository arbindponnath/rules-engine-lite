import DecisionTable from '../models/decisionTable';

/**
 * Class representation of DecisionTable controller.
 */

export default class DecisionTableController {
  static create = async (req, res, next) => {
    try {
      const {
        name,
        decisionRules,
        documentData,
        documentName,
        dtDesignerData,
      } = req.body;
      const decisionTableObj = new DecisionTable({
        name,
        decisionRules,
        documentData,
        documentName,
        dtDesignerData,
      });
      if (typeof decisionTableObj === 'object') {
        await decisionTableObj.createDecisionTable();
        res.status(200).json({ name });
      } else throw new Error('Error creating decision table object.');
    } catch (error) {
      next(error);
    }
  };

  static findAll = async (req, res, next) => {
    try {
      const resp = await DecisionTable.findAllDecisionTables();
      res.status(200).json(resp);
    } catch (error) {
      next(error);
    }
  };

  static findById = async (req, res, next) => {
    try {
      const id = req.params['id'];
      const resp = await DecisionTable.findDecisionTableById();
      res.status(200).json(resp);
    } catch (error) {
      next(error);
    }
  };

  static exec = async (req, res, next) => {
    try {
      const { name, amount, type, experience, monthlyIncome } = req.body;
      const resp = await DecisionTable.exec(name, {
        amount,
        type,
        experience,
        monthlyIncome,
      });
      res.status(200).json(resp);
    } catch (error) {
      next(error);
    }
  };
}
