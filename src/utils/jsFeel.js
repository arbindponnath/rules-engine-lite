import jsFeel from 'js-feel';

/**
 * Class representation of js-feel
 */
export default class JsFeelUtils {
  static getDecision(id, decisionRules, payload) {
    return new Promise((resolve, reject) => {
      jsFeel().decisionTable.execute_decision_table(
        id,
        decisionRules,
        payload,
        function (err, resp) {
          if (err) reject(err);
          else {
            resolve(resp);
          }
        }
      );
    });
  }
}
