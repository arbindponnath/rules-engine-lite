# rule-engine-lite project

A light weight rule engine based on js-feel.

### Install Dependencies

```sh
$ git clone https://gitlab.com/arbindponnath/rules-engine-lite
$ cd rules-engine-lite
$ npm install
```

### Starting the application

```sh
$ npm start
```

Web server listening at: http://localhost:8081
